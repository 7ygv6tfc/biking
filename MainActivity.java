package com.example.biking;


import android.app.Activity;
import android.view.*;
import android.widget.ImageButton;
import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		startActivity(new Intent(this, IntroActivity.class));
		
		ImageButton top5;
		ImageButton region;
		
		top5 = (ImageButton) findViewById(R.id.top5);
		region = (ImageButton) findViewById(R.id.region);
		
		top5.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, Top5.class);
				startActivity(intent);
			}
	});
}
}
